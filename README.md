# OpenML dataset: Credit_Card_Fraud_Detection

https://www.openml.org/d/44235

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

It is important that credit card companies are able to recognize fraudulent credit card transactions so that customers are not charged for items that they did not purchase.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44235) of an [OpenML dataset](https://www.openml.org/d/44235). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44235/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44235/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44235/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

